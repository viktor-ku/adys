FROM node:8-alpine

EXPOSE 3000
WORKDIR /home/node/app

ENV NODE_ENV production
ENV NODE_PORT 3000

RUN apk update && apk add tzdata && cp /usr/share/zoneinfo/Europe/Tallinn /etc/localtime && echo "Europe/Tallinn" > /etc/timezone && apk del tzdata

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY dist dist

USER node

CMD npm run up && npm start
