// @flow
'use strict'

const t = require('tap')
const parseUrl = require('../../src/lib/parseUrl')

t.test('parseUrl', t => {
  t.test('ok', t => {
    const expected = {
      countries: ['US', 'RU'],
      categories: ['Automobile'],
      bid: 0.1
    }

    const links = [
      '?countrycode=US,RU&Category=Automobile&BaseBid=10',
      '?CountryCode=US,RU&Category=Automobile&basebid=10'
    ]

    links.map(parseUrl).forEach(actual => {
      t.ok(actual)
      t.notOk(actual.errors)

      t.equal(typeof actual.data.bid, 'number')

      t.deepEqual(actual.data, expected)
    })

    t.end()
  })

  t.test('has errors', t => {
    const errors = {
      country: 'Country Code is required',
      category: 'Category is required',
      bid: 'Base Bid is required'
    }

    const links = [
      [
        '',
        [errors.country, errors.category, errors.bid]
      ],
      [
        '?item=banana&count=1&price=3.45',
        [errors.country, errors.category, errors.bid]
      ],
      [
        '?countri=US&category=Automobile&bid=10',
        [errors.country, errors.bid]
      ],
      [
        '?CountryCode=US&Category=Automobile&Bid=10.11s',
        [errors.bid]
      ]
    ]

    links.forEach(([url: string, errors: Array<string>]) => {
      const actual = parseUrl(url)

      t.ok(actual.errors)
      t.notOk(actual.data)

      t.deepEqual(actual.errors, errors, url)
    })

    t.end()
  })

  t.end()
})
