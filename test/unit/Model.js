// @flow
'use strict'

const t = require('tap')
const { ObjectId } = require('mongodb')
const Model = require('../../src/models/Model')

t.test('Model', t => {
  t.ok(Model.isString('foo'))
  t.notOk(Model.isString(13))
  t.ok(Model.isNumber(13))
  t.notOk(Model.isNumber('NaN'))
  t.ok(Model.isBoolean(true))
  t.ok(Model.isBoolean(false))
  t.notOk(Model.isBoolean('banana'))
  t.notOk(Model.isBoolean(13))
  t.ok(Model.isObjectId(ObjectId()))
  t.notOk(Model.isObjectId(ObjectId().toString()))
  t.notOk(Model.isObjectId('banana'))
  t.ok(Model.isObjectIdArray([ObjectId(), ObjectId()]))
  t.notOk(Model.isObjectIdArray([ObjectId().toString()]))
  t.notOk(Model.isObjectIdArray(['banana', 13]))

  const a = new Model()
  t.ok(Model.isObjectId(a._id))

  const _id = ObjectId()

  const b = new Model({ _id })
  t.ok(Model.isObjectId(b._id))
  t.ok(_id.equals(b._id))

  const c = new Model({ _id: _id.toString() })
  t.ok(Model.isObjectId(c._id))
  t.notOk(_id.equals(c._id))
  t.equal(c._id.getTimestamp().toString(), c.createdAt.toString())

  t.end()
})
