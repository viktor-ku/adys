// @flow
'use strict'

const { ObjectId } = require('mongodb')

class Model {
  _id: ObjectId

  constructor ({ _id }: Object = {}) {
    this._id = Model.isObjectId(_id) ? _id : ObjectId()
  }
  get createdAt (): Date {
    return this._id.getTimestamp()
  }

  static isString (x): boolean {
    return typeof x === 'string'
  }
  static isNumber (x): boolean {
    return typeof x === 'number'
  }
  static isBoolean (x): boolean {
    return x === true || x === false
  }
  static isObjectId (x): boolean {
    return !!x && ObjectId.isValid(x) && typeof x === 'object'
  }
  static isObjectIdArray (x): boolean {
    return Array.isArray(x) && x.every(_id => Model.isObjectId(_id))
  }
}

module.exports = Model
