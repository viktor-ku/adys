// @flow
'use strict'

const { MongoClient } = require('mongodb')
const Koa = require('koa')
const Router = require('koa-router')
const config = require('./lib/config')
const pkg = require('../package.json')

const routes = {
  exchange: require('./route/exchange'),
  dbSnapshot: require('./route/dbSnapshot')
}

const app = new Koa()
const router = new Router()

if (!config.isProduction) {
  app.use(require('koa-logger')())
}

router
  .get('/', routes.exchange.get)
  .get('/snapshot/db', routes.dbSnapshot.get)

app
  .use(router.routes())
  .use(router.allowedMethods())

async function main () {
  const db = await MongoClient.connect(config.mongoUrl)

  app.context.db = db
  app.context.$companies = db.collection('companies')
  app.context.$countries = db.collection('countries')
  app.context.$categories = db.collection('categories')

  app.listen(config.port, () => {
    if (config.isProduction) {
      console.log('%s (v%s) listening on port :%s', pkg.name, pkg.version, config.port)
    } else {
      console.log('%s (dev) listening on port :%s', pkg.name, config.port)
    }
  })
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
