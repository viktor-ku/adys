// @flow
'use strict'

exports.get = async (x: Object) => {
  const categories = await x.$categories.find().toArray()
  const categoryMap = new Map()
  categories.forEach(category => {
    categoryMap.set(category._id.toString(), category)
  })

  const countries = await x.$countries.find().toArray()
  const countryMap = new Map()
  countries.forEach(country => {
    countryMap.set(country._id.toString(), country)
  })

  let companies = await x.$companies.find().toArray()

  companies = companies
    .map(company => Object.assign({}, company, {
      categories: company.categories.map(categoryId => categoryMap.get(categoryId.toString())),
      countries: company.countries.map(countryId => countryMap.get(countryId.toString()))
    }))

  x.body = {
    countries,
    categories,
    companies
  }
}
