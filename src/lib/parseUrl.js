// @flow
'use strict'

const parseUrl = require('url').parse

type Url = {
  countries: Array<string>,
  categories: Array<string>,
  bid: number
}

type Actual = {
  data?: Url,
  errors?: Array<string>
}

module.exports = (fullUrl: string): Actual => {
  const errors = []
  const { query } : Object = parseUrl(fullUrl, true)

  const normUrl = {}
  Object.keys(query).forEach(key => {
    const lowerKey: string = key.toLowerCase()
    const value: Array<string> = query[key].split(',')

    normUrl[lowerKey] = lowerKey === 'basebid' ? value.join() : value
  })

  const countries = normUrl.countrycode
  const categories = normUrl.category
  const bid = Number(normUrl.basebid) / 1e2

  if (!Array.isArray(countries) || !countries.length) {
    errors.push('Country Code is required')
  }
  if (!Array.isArray(categories) || !categories.length) {
    errors.push('Category is required')
  }
  if (!bid || isNaN(bid)) {
    errors.push('Base Bid is required')
  }

  if (errors.length) {
    return {
      errors
    }
  }

  return {
    data: {
      countries,
      categories,
      bid
    }
  }
}
